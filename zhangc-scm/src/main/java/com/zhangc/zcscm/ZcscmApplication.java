package com.zhangc.zcscm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZcscmApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZcscmApplication.class, args);
    }

}
