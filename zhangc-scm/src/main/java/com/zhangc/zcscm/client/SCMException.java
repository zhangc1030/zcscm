/*
 *
 * FileName: SCMException.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 10:21
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.client;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class SCMException extends RuntimeException {
    private static final long serialVersionUID = -567786786121301727L;

    public SCMException(String message) {
        super(message);
    }

    public SCMException(String message, Throwable cause) {
        super(message, cause);
    }
}
