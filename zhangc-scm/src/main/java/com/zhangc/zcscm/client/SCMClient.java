/*
 *
 * FileName: SCMClient.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 10:20
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.client;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public interface SCMClient {
    SCMNode getConfig(String var1);

    int createConfig(String var1, String var2, String var3) throws SCMException;

    int updateConfig(String var1, String var2, String var3) throws SCMException;

    int deleteConfig(String var1, String var2) throws SCMException;

    void destroy();

    void destroySCMNode(String var1);

    void destroySCMNodeByAPPCode(String var1, String var2);

    String getAppCode();

    String getSecureKey(String var1);

    SCMNode getGlobalConfig(String var1) throws SCMException;

    SCMClient.Environment getEnvironment();

    SCMNode readConfig(String var1, String var2);

    int updateGlobalConfig(String var1, String var2) throws SCMException;

    int createConfigByApp(String var1, String var2, String var3) throws SCMException;

    int updateConfigByApp(String var1, String var2, String var3) throws SCMException;

    int deleteConfigByApp(String var1, String var2) throws SCMException;

    String updateSwitchConfig(String var1, String var2, String var3, String var4);

    String checkSwitchConfig(String var1, String var2, String var3);

    public static enum Environment {
        PRD,
        NOT_PRD;

        private Environment() {
        }
    }
}
