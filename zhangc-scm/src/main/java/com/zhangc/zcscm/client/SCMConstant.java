package com.zhangc.zcscm.client;

public class SCMConstant {
    public static final String C = "create";
    public static final String U = "update";
    public static final String D = "delete";
    public static final String SCM_SERVER_DEV = "http://scmdev";
    public static final String SCM_SERVER_SIT = "http://scmsit";
    public static final String SCM_SERVER_PRE = "http://scmpre";
    public static final String SCM_SERVER_XG_PRE = "http://scmxgpre";
    public static final String SCM_SERVER_PST = "http://scmpst";
    public static final String SCM_SERVER_POC = "http://scmpoc";
    public static final String ENCODE = "UTF-8";
    public static final String FILE_PATH = System.getProperty("user.home");
    public static final String REGEX = "[`~!@#$%^&*()+=|{}':;',//[//].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
    public static final String SYSTEMCODE = "SCM";

    public SCMConstant() {
    }
}
