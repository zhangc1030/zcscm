package com.zhangc.zcscm.util;

import java.lang.management.ManagementFactory;

import org.apache.commons.lang3.StringUtils;

public class ProcessUtils {
    private static volatile String PROCESS_ID = null;

    public ProcessUtils() {
    }

    private static void initProcessId() {
        try {
            String process = ManagementFactory.getRuntimeMXBean().getName();
            if (!"".equals(process) && null != process) {
                PROCESS_ID = StringUtils.replace(process, "@", "");
            }
        } catch (Throwable var1) {
        }

    }

    public static String getProcessId() {
        if (StringUtils.isEmpty(PROCESS_ID)) {
            initProcessId();
        }

        return PROCESS_ID;
    }

    static {
        initProcessId();
    }
}
