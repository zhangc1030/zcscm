package com.zhangc.zcscm.util;

import java.lang.reflect.Method;

public class ServerNameGetter {
    private static String serverName = System.getProperty("jboss.server.name");

    public ServerNameGetter() {
    }

    public static String getServerName() {
        return serverName;
    }

    static {
        if (serverName == null || serverName.equals("")) {
            try {
                Class<?> c = Class.forName("com.ibm.websphere.runtime.ServerName");
                Method m = c.getMethod("getDisplayName");
                Object o = m.invoke(c);
                serverName = o.toString();
            } catch (Exception var3) {
                serverName = "defaultServer";
            }
        }

        if (serverName == null || serverName.equals("")) {
            serverName = "defaultServer";
        }

    }
}
