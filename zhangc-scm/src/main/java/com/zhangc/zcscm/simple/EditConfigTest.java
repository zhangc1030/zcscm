/*
 *
 * FileName: EditConfigTest.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 16:49
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.simple;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class EditConfigTest {
    public static void main(String[] args) {
        //addConfig();
        updateConfig();
    }

    public static void addConfig() {
        ConfigServer configServer = new ConfigServer();
        configServer.createRoot();
        configServer.write("jdbc.url", "com.mysql.jdbc.Driver");
        configServer.write("jdbc.password", "1234");
        //configServer.close();
    }

    public static void updateConfig() {
        ConfigServer configServer = new ConfigServer();
        configServer.write("jdbc.url", "com.mysql.jdbc.update.test21111111111111");
        //configServer.close();
    }
}
