/*
 *
 * FileName: ConfigClient.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 16:43
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.simple;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ConfigClient implements Watcher {
    Logger logger = LoggerFactory.getLogger(ConfigClient.class);
    private ConfigServer configServer = new ConfigServer();
    private CountDownLatch countDownLatch = new CountDownLatch(1);

    public void process(WatchedEvent watchedEvent) {
        String path = watchedEvent.getPath();
        if (watchedEvent.getType() == Event.EventType.NodeDataChanged) {
            this.updateConfig(path);
        }
    }

    public void updateConfig(String path) {
        logger.info(configServer.readConfig(path.substring(path.lastIndexOf("/") + 1), this));
        //        System.out.println(configServer.readConfig(path.substring(path.lastIndexOf("/") + 1), this));
    }

    public void getAllConfig() {
        Map<String, String> map = configServer.getAllConfig();
        Iterator<String> keys = map.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            String value = map.get(key);
            System.out.println("key:" + key + "  value:" + value);
        }
    }
}
