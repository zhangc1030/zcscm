/*
 *
 * FileName: SCMController.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 15:46
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zhangc.zcscm.ScmConfUtil;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@RequestMapping(value = "scm")
@RestController
public class SCMController {

    @RequestMapping(value = "/read")
    public String read() {
        return ScmConfUtil.getInstance().getString("carr_tp_order_cancle_param", "111");
    }
}
