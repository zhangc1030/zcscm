/*
 *
 * FileName: ZkLock.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 10:44
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.zookeeper;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ZkLock extends ReentrantLock {
    private static final long serialVersionUID = 1L;
    private Condition _dataChangedCondition = this.newCondition();
    private Condition _stateChangedCondition = this.newCondition();
    private Condition _zNodeEventCondition = this.newCondition();

    public ZkLock() {
    }

    public Condition getDataChangedCondition() {
        return this._dataChangedCondition;
    }

    public Condition getStateChangedCondition() {
        return this._stateChangedCondition;
    }

    public Condition getZNodeEventCondition() {
        return this._zNodeEventCondition;
    }
}
