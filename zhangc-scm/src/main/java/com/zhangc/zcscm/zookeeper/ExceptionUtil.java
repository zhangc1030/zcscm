/*
 *
 * FileName: ExceptionUtil.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 11:45
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.zookeeper;

import com.zhangc.zcscm.zookeeper.exception.ZkInterruptedException;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ExceptionUtil {
    public ExceptionUtil() {
    }

    public static RuntimeException convertToRuntimeException(Throwable e) {
        if (e instanceof RuntimeException) {
            return (RuntimeException) e;
        } else {
            retainInterruptFlag(e);
            return new RuntimeException(e);
        }
    }

    public static void retainInterruptFlag(Throwable catchedException) {
        if (catchedException instanceof InterruptedException) {
            Thread.currentThread().interrupt();
        }

    }

    public static void rethrowInterruptedException(Throwable e) throws InterruptedException {
        if (e instanceof InterruptedException) {
            throw (InterruptedException) e;
        } else if (e instanceof ZkInterruptedException) {
            throw (ZkInterruptedException) e;
        }
    }
}