/*
 *
 * FileName: IZkStateListener.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 10:43
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.zookeeper;

import org.apache.zookeeper.Watcher;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public interface IZkStateListener {
    void handleStateChanged(Watcher.Event.KeeperState var1) throws Exception;

    void handleNewSession() throws Exception;

    void handleSessionEstablishmentError(Throwable var1) throws Exception;
}
