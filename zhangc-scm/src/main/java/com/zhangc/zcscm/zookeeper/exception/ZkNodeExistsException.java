/*
 *
 * FileName: ZkNodeExistsException.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 11:44
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.zookeeper.exception;

import org.apache.zookeeper.KeeperException;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ZkNodeExistsException extends ZkException {
    private static final long serialVersionUID = 1L;

    public ZkNodeExistsException() {
    }

    public ZkNodeExistsException(KeeperException cause) {
        super(cause);
    }

    public ZkNodeExistsException(String message, KeeperException cause) {
        super(message, cause);
    }

    public ZkNodeExistsException(String message) {
        super(message);
    }
}
