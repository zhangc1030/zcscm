/*
 *
 * FileName: ZkException.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 11:16
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.zookeeper.exception;

import org.apache.zookeeper.KeeperException;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ZkException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ZkException() {
    }

    public ZkException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZkException(String message) {
        super(message);
    }

    public ZkException(Throwable cause) {
        super(cause);
    }

    public static ZkException create(KeeperException e) {
        switch (e.code()) {
            case NONODE:
                return new ZkNoNodeException(e);
            case BADVERSION:
                return new ZkBadVersionException(e);
            //            case NODEEXISTS:
            //                return new ZkNodeExistsException(e);
            default:
                return new ZkException(e);
        }
    }
}
