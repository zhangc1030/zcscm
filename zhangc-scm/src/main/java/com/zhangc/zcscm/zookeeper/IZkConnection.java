/*
 *
 * FileName: IZkConnection.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/24 10:42
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.zcscm.zookeeper;

import java.util.List;
import java.util.Map;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.Op;
import org.apache.zookeeper.OpResult;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public interface IZkConnection {
    void connect(Watcher var1);

    void reconnect(Watcher var1) throws InterruptedException;

    void close() throws InterruptedException;

    String create(String var1, byte[] var2, CreateMode var3) throws KeeperException, InterruptedException;

    String create(String var1, byte[] var2, List<ACL> var3, CreateMode var4) throws KeeperException, InterruptedException;

    void delete(String var1) throws InterruptedException, KeeperException;

    void delete(String var1, int var2) throws InterruptedException, KeeperException;

    boolean exists(String var1, boolean var2) throws KeeperException, InterruptedException;

    List<String> getChildren(String var1, boolean var2) throws KeeperException, InterruptedException;

    byte[] readData(String var1, Stat var2, boolean var3) throws KeeperException, InterruptedException;

    void writeData(String var1, byte[] var2, int var3) throws KeeperException, InterruptedException;

    Stat writeDataReturnStat(String var1, byte[] var2, int var3) throws KeeperException, InterruptedException;

    ZooKeeper.States getZookeeperState();

    long getCreateTime(String var1) throws KeeperException, InterruptedException;

    String getServers();

    List<OpResult> multi(Iterable<Op> var1) throws KeeperException, InterruptedException;

    void setAcl(String var1, List<ACL> var2, int var3) throws KeeperException, InterruptedException;

    Map.Entry<List<ACL>, Stat> getAcl(String var1) throws KeeperException, InterruptedException;

    long getSessionId();
}
