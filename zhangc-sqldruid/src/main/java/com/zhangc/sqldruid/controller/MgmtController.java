package com.zhangc.sqldruid.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 功能描述:<br>
 * 详细功能描述
 *
 * @author 17121439
 * @see [相关类/方法]（可选）
 * @since [产品 /模块版本] （可选）
 */
@Controller
@RequestMapping("/mgmt")
public class MgmtController {

    /**
     * 功能描述: <br>
     * 〈功能详细描述〉
     *
     * @param request request
     * @return string
     * @author 17121439
     */
    @RequestMapping("dataMgmt.do")
    public String dataMgmt(HttpServletRequest request) {
        return "dataMgmt1";
        //        String soaUserId = request.getRemoteUser();
        //        //String mgmtUser = ScmConfUtil.getInstance().getString("mgmtUser", "zc");
        //        String mgmtUser = "";
        //        if (Arrays.asList(mgmtUser.split(",")).contains(soaUserId)) {
        //            return "dataMgmt";
        //        }
        //        return "error";
    }

    /**
     * 功能描述: <br>
     * 〈功能详细描述〉
     *
     * @param request request
     * @return string
     * @author 17121439
     */
    @RequestMapping("jedisMgmt.do")
    public String jedisMgmt(HttpServletRequest request) {
        String soaUserId = request.getRemoteUser();
        // String mgmtUser = ScmConfUtil.getInstance().getString("mgmtUser", "zc");
        String mgmtUser = "";
        if (Arrays.asList(mgmtUser.split(",")).contains(soaUserId)) {
            return "jedisMgmt";
        }
        return "error";
    }
}
