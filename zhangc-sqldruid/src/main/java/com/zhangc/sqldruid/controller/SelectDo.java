package com.zhangc.sqldruid.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述:<br>
 *
 * @author
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Configuration
public class SelectDo {
    @Bean(name = "/mgmt/sql/select.do")
    public SQLSelectHttpRequestHandler getMapper() {
        return new SQLSelectHttpRequestHandler();
    }
}
