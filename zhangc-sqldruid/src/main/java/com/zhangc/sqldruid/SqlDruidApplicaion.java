package com.zhangc.sqldruid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能描述:<br>
 *
 * @author wx:zhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@SpringBootApplication
public class SqlDruidApplicaion {
    public static void main(String[] args) {
        SpringApplication.run(SqlDruidApplicaion.class, args);
    }
}
