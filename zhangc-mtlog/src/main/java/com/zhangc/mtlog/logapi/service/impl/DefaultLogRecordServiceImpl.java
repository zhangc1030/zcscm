package com.zhangc.mtlog.logapi.service.impl;

import java.util.List;

import com.google.common.collect.Lists;
import com.zhangc.mtlog.logapi.beans.LogRecord;
import com.zhangc.mtlog.logapi.service.ILogRecordService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultLogRecordServiceImpl implements ILogRecordService {

    //    @Resource
    //    private LogRecordMapper logRecordMapper;

    @Override
    //    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void record(LogRecord logRecord) {
        log.info("【logRecord】log={}", logRecord);
        //        logRecordMapper.insertSelective(logRecord);
    }

    @Override
    public List<LogRecord> queryLog(String bizKey) {
        //        return logRecordMapper.queryByBizKey(bizKey);
        return Lists.newArrayList();
    }

    @Override
    public List<LogRecord> queryLogByBizNo(String bizNo) {

        //        return logRecordMapper.queryByBizNo(bizNo);
        return Lists.newArrayList();
    }
}
