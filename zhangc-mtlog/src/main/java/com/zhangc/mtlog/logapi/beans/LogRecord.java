package com.zhangc.mtlog.logapi.beans;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LogRecord {
    private Integer id;
    private String tenant;
    private String bizKey;

    private String bizNo;

    private String operator;

    private String action;

    private String category;
    private Date createTime;
    private String detail;
}