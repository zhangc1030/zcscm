package com.zhangc.mtlog.logapi.service;

public interface IParseFunction {

    default boolean executeBefore() {
        return false;
    }

    String functionName();

    String apply(String value);
}
