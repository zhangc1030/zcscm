package com.zhangc.mtlog.logapi.starter.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface LogRecordAnnotation {
    //操作日志 成功 的文本模板
    String success();

    //操作日志 失败 的文本模板
    String fail() default "";

    //操作日志的 执行人呢
    String operator() default "";

    //业务对象标识 前缀
    String prefix();

    //业务对象标识
    String bizNo();

    //业务日志的种类
    String category() default "";

    //扩展参数
    String detail() default "";

    //记录日志的条件
    String condition() default "";
}
