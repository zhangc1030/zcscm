package com.zhangc.mtlog.logserver.service;

import java.util.List;

import com.zhangc.mtlog.logserver.beans.UserDto;

/**
 * @author muzhantong
 * create on 2021/2/10 11:13 上午
 */
public interface UserQueryService {
    List<UserDto> getUserList(List<String> userIds);
}
