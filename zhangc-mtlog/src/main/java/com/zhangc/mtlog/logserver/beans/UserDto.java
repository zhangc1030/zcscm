package com.zhangc.mtlog.logserver.beans;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Data
@NoArgsConstructor
public class UserDto {
    private String Username;
}
