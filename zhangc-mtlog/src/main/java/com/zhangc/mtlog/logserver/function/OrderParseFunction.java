package com.zhangc.mtlog.logserver.function;

import javax.annotation.Resource;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.zhangc.mtlog.logapi.service.IParseFunction;
import com.zhangc.mtlog.logserver.beans.Order;
import com.zhangc.mtlog.logserver.service.OrderQueryService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrderParseFunction implements IParseFunction {
    @Resource
    @Lazy
    private OrderQueryService orderQueryService;

    @Override
    public boolean executeBefore() {
        return true;
    }

    @Override
    public String functionName() {
        return "ORDER";
    }

    @Override
    public String apply(String value) {
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        log.info("###########,{}", value);
        Order order = orderQueryService.queryOrder(Long.parseLong(value));
        return order.getProductName().concat("(").concat(value).concat(")");
    }
}
