package com.zhangc.mtlog.logserver.service;

import com.zhangc.mtlog.logserver.beans.Order;

public interface OrderQueryService {
    Order queryOrder(long parseLong);
}
