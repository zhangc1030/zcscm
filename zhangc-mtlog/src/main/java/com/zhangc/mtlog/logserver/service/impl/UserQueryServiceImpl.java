package com.zhangc.mtlog.logserver.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.zhangc.mtlog.logapi.context.LogRecordContext;
import com.zhangc.mtlog.logapi.starter.annotation.LogRecordAnnotation;
import com.zhangc.mtlog.logserver.beans.UserDto;
import com.zhangc.mtlog.logserver.constants.LogRecordType;
import com.zhangc.mtlog.logserver.service.UserQueryService;

@Service
public class UserQueryServiceImpl implements UserQueryService {

    @Override
    @LogRecordAnnotation(success = "获取用户列表,内层方法调用人{{#user}}", prefix = LogRecordType.ORDER, bizNo = "{{#user}}")
    public List<UserDto> getUserList(List<String> userIds) {
        LogRecordContext.putVariable("user", "mzt");
        return null;
    }
}
