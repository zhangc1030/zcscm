package com.zhangc.mtlog.logserver.service;

import com.zhangc.mtlog.logserver.beans.Order;

public interface IOrderService {
    boolean createOrder(Order order);

    boolean update(Long orderId, Order order);

    boolean testCondition(Long orderId, Order order, String condition);

    boolean testContextCallContext(Long orderId, Order order);
}
