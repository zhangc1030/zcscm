package com.zhangc.mtlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import com.zhangc.mtlog.logapi.starter.annotation.EnableLogRecord;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
//@EnableTransactionManagement
@EnableLogRecord(tenant = "com.mzt.test")
public class MtLogApplicaion {
    public static void main(String[] args) {
        SpringApplication.run(MtLogApplicaion.class, args);
    }
}
