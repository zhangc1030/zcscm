package com.zhangc.batch;

import java.util.List;
import java.util.concurrent.ExecutorService;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BatchContext<I, O> {

    public BatchContext() {
    }

    public BatchContext(List<I> batchList) {
        this.batchList = batchList;
    }

    public BatchContext(List<I> batchList, boolean aysn) {
        this.batchList = batchList;
        this.aysn = aysn;
    }

    private boolean aysn;

    private List<I> batchList;

    private BatchPropertySetter<I> propertySetter;

    private BatchDataHandle<I, O> batchDataHandle;

    private ExecutorService executorService;

    private int timeout = 60;
}
