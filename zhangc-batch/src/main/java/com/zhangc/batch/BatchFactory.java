package com.zhangc.batch;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

@Component
public class BatchFactory implements InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchFactory.class);
    //    @Autowired
    //    private QueryOrdersUtils queryOrdersUtils;
    private ImmutableMap<String, BatchConvert> converMap = null;

    //
    public interface BatchConvert {
        void apply(BatchBean batchBean);
    }

    // 默认
    private static final BatchConvert defaultCovert = new BatchConvert() {
        @Override
        public void apply(BatchBean batchBean) {
        } // donothing
    };

    public BatchConvert getBatchConvert(String key) {
        return converMap.getOrDefault(key, defaultCovert);
    }

    //异步执行的单个业务
    public void applyOrder(BatchBean batchBean, String orderId) {
        //batchBean.getOrderMap().put(orderId, queryOrdersUtils.getOrderDetailByOrderNo(orderId, batchBean.getStoreCode()));
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, BatchConvert> map = Maps.newHashMap();
        converMap = ImmutableMap.copyOf(map);
    }
}
