package com.zhangc.batch;

import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.zhangc.batch.dto.ResultDto;

/**
 * 功能描述:<br>
 * 传递入参、出参
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品 /模块版本] （可选）
 */
public class BatchBean {

    public BatchBean() {
    }

    private Set<String> orderIdSet = Sets.newHashSet();

    private String storeCode;

    /**
     * Gets the value of storeCode
     *
     * @return the value of storeCode
     */
    public String getStoreCode() {
        return storeCode;
    }

    /**
     * Sets the storeCode
     *
     * @param storeCode storeCode
     */
    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    // 以下信息 通过上述属性获取 ==============================================
    private Map<String, ResultDto> orderMap = Maps.newHashMap();

    /**
     * Gets the value of orderIdSet
     *
     * @return the value of orderIdSet
     */
    public Set<String> getOrderIdSet() {
        return orderIdSet;
    }

    /**
     * Sets the orderIdSet
     *
     * @param orderIdSet orderIdSet
     */
    public void setOrderIdSet(Set<String> orderIdSet) {
        this.orderIdSet = orderIdSet;
    }

    /**
     * Gets the value of orderMap
     *
     * @return the value of orderMap
     */
    public Map<String, ResultDto> getOrderMap() {
        return orderMap;
    }

    /**
     * Sets the orderMap
     *
     * @param orderMap orderMap
     */
    public void setOrderMap(Map<String, ResultDto> orderMap) {
        this.orderMap = orderMap;
    }
}
