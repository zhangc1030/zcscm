/*
 *
 * FileName: BatchInParam.java
 * Author:   wx:fdzhangc
 * Date:     2021/09/02 9:36
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.batch.dto;

import java.io.Serializable;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class BatchInParam implements Serializable {
    public BatchInParam(String orderId, String storeCode) {
        this.orderId = orderId;
        this.storeCode = storeCode;
    }

    private static final long serialVersionUID = 7418933821411268296L;
    private String orderId;
    private String storeCode;

    /**
     * Gets the value of orderId
     *
     * @return the value of orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the orderId
     *
     * @param orderId orderId
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * Gets the value of storeCode
     *
     * @return the value of storeCode
     */
    public String getStoreCode() {
        return storeCode;
    }

    /**
     * Sets the storeCode
     *
     * @param storeCode storeCode
     */
    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }
}
