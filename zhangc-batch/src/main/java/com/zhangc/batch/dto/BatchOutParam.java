package com.zhangc.batch.dto;

import java.io.Serializable;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class BatchOutParam implements Serializable {
    private static final long serialVersionUID = 7418933821411268296L;
    private String orderId;
    private ResultDto resultDto;

    /**
     * Gets the value of orderId
     *
     * @return the value of orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the orderId
     *
     * @param orderId orderId
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * Gets the value of resultDto
     *
     * @return the value of resultDto
     */
    public com.zhangc.batch.dto.ResultDto getResultDto() {
        return resultDto;
    }

    /**
     * Sets the resultDto
     *
     * @param resultDto resultDto
     */
    public void setResultDto(com.zhangc.batch.dto.ResultDto resultDto) {
        this.resultDto = resultDto;
    }
}
