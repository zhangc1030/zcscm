package com.zhangc.batch;

public interface BatchDataHandle<I, O> {

    O transform(I i, BatchBean batchBean);
}