package com.zhangc.batch;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.zhangc.batch.dto.BatchInParam;
import com.zhangc.batch.dto.BatchOutParam;
import com.zhangc.batch.dto.ResultDto;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Service
public class BatchDemoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchDemoService.class);
    @Autowired
    BatchService batchService;

    /**
     * 功能描述: <br>
     * 〈批量异步查询 omsq001 订单信息〉<br>
     * 入参：订单号
     *
     * @param params params
     * @return void
     * @author wx:fdzhangc
     */
    public Map<String, ResultDto> batchAsynQuery001(List<BatchInParam> params) {

        Set<String> level3 = ImmutableSet.of(BatchConstant.ORDER_OMSQ_001);
        //        boolean aysnQuery001Scm = ScmConfUtil.getInstance().getBoolean(ScmConstants.OMSQ_001_AYSN, ScmConstants.DEFAULT_OMSQ_001_AYSN);
        BatchContext<BatchInParam, BatchOutParam> context = new BatchContext<>(params, true);

        context.setPropertySetter(new BatchPropertySetter<BatchInParam>() {
            @Override
            public void apply(BatchInParam testIn, BatchBean batchBean) {
                batchBean.getOrderIdSet().add(testIn.getOrderId());
                batchBean.setStoreCode(testIn.getStoreCode());
            }
        });

        context.setBatchDataHandle(new BatchDataHandle<BatchInParam, BatchOutParam>() {
            @Override
            public BatchOutParam transform(BatchInParam testIn, BatchBean batchBean) {
                BatchOutParam testOut = new BatchOutParam();
                testOut.setOrderId(testIn.getOrderId());
                testOut.setResultDto(batchBean.getOrderMap().get(testIn.getOrderId()));
                return testOut;
            }
        });
        List<BatchOutParam> list = batchService.batchTransformData(ImmutableList.of(level3), context);
        if (CollectionUtils.isEmpty(list)) {
            return Maps.newHashMap();
        }
        list = list.stream().filter(a -> StringUtils.isNotBlank(a.getOrderId()) && a.getResultDto() != null)
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(list)) {
            return Maps.newHashMap();
        }
        return list.stream().collect(Collectors.toMap(BatchOutParam::getOrderId, BatchOutParam::getResultDto, (v1, v2) -> v1));
    }
}
