package com.zhangc.batch;

public interface BatchPropertySetter<I> {

    void apply(I i, BatchBean batchBean);
}
