package com.zhangc.limiter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述:<br>
 *
 * @author
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Configuration
public class FilterRegister {
    //定义一个bean
    @Bean
    public FilterRegistrationBean registFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new RateLimiterFilter());
        registrationBean.addUrlPatterns("/*");
        registrationBean.setName("RateLimiterFilter");
        registrationBean.setOrder(1);
        return registrationBean;
    }
}
