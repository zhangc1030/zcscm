package com.zhangc.limiter;

public enum RateLimiterEnum {
    /**
     * 链接纬度超阀值
     */
    WEB(" 链接纬度超阀值"),
    /**
     * 用户纬度超阀值
     */
    USER(" 用户纬度超阀值"),
    /**
     * IP纬度超阀值
     */
    IP(" IP纬度超阀值");
    /**
     * The Value.
     */
    final String value;

    /**
     * Instantiates a new Rate limiter enum.
     *
     * @param value the value
     */
    RateLimiterEnum(String value) {
        this.value = value;
    }

    /**
     * 功能描述: <br>
     * 〈功能详细描述〉
     *
     * @return string
     * @author 17040686
     */
    public String value() {
        return this.value;
    }

}
