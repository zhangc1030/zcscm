package easyexcel.exception;

public class FileStopException extends RuntimeException {

    public FileStopException(String msg) {
        super(msg);
    }
}
