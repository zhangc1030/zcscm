package easyexcel;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import easyexcel.exportexcel.EasyExcelExport;
import easyexcel.exportexcel.EasyExportBean;
import easyexcel.exportexcel.EasyExportHandle;
import easyexcel.exportexcel.ExportTaskBean;
import easyexcel.importexcel.EasyImportBean;
import easyexcel.importexcel.EasyImportHandle;
import easyexcel.importexcel.ImportBatchBean;
import easyexcel.importexcel.ImportTaskBean;

/**
 * 本类为测试类，根据自己实际业务去调整
 */
@Service
public class TestExecuteSerivceImpl {

    @Autowired
    CommonExcelService commonExcelService;

    //    @Autowired
    //    TestDao testDao;

    public void load(Integer bizType, Integer taskType, Long id) {
        // 测试代码
        new Thread(new Runnable() {
            @Override
            public void run() {
                EasyImportBean<TestImportBean> easyImportBean = new EasyImportBean<>();
                easyImportBean.setImportId(id);
                easyImportBean.setClz(TestImportBean.class);
                easyImportBean.setHandle(new EasyImportHandle<TestImportBean>() {
                    @Override
                    public ImportBatchBean<TestImportBean> handle(List<TestImportBean> rowList, ImportTaskBean importTaskBean) {
                        ImportBatchBean<TestImportBean> batchBean = new ImportBatchBean<>(rowList.size());

                        List<TestImportBean> errorList = Lists.newArrayList();
                        Iterator<TestImportBean> iterable = rowList.iterator();
                        while (iterable.hasNext()) {
                            TestImportBean testBean = iterable.next();

                            if (testBean.getId() % 2 == 0) {
                                errorList.add(new TestImportBean(testBean.getId(), testBean.getName(), "id不能为偶数"));
                                iterable.remove();
                            }
                        }
                        //testDao.insert(rowList);
                        batchBean.setErrorList(errorList);
                        return batchBean;
                    }
                });
                commonExcelService.easyImport(easyImportBean);
            }
        }).start();
    }

    public void export(Integer bizType, Integer taskType, Long id) {
        // 测试代码
        new Thread(new Runnable() {
            @Override
            public void run() {
                EasyExportBean<TestExportBean, Integer> easyExportBean = new EasyExportBean<>();
                easyExportBean.setExportId(id);
                easyExportBean.setFileName("test" + System.currentTimeMillis());
                easyExportBean.setSheetName("test");
                easyExportBean.setClz(TestExportBean.class);
                easyExportBean.setHandle(new EasyExportHandle<TestExportBean, Integer>() {
                    @Override
                    public List<TestExportBean> handle(EasyExcelExport.ExportContext<TestExportBean, Integer> context,
                            ExportTaskBean taskBean) {
                        return null;
                        //return testDao.list(context.getPageNo(), context.getPageSize());
                    }
                });
                commonExcelService.easyExport(easyExportBean);
            }
        }).start();
    }
}
