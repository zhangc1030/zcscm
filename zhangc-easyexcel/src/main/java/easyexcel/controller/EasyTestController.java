/*
 *
 * FileName: EasyTestController.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/25 18:22
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package easyexcel.controller;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import easyexcel.TestExecuteSerivceImpl;
import easyexcel.exportexcel.ExportTaskBean;
import easyexcel.importexcel.ImportConfigBean;
import easyexcel.importexcel.ImportFileReceipt;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class EasyTestController {

    @Autowired
    TestExecuteSerivceImpl testExecuteSerivce;
    @Autowired
    ImportFileReceipt importFileReceipt;

    @RequestMapping(value = "import.do", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject importFile(@RequestParam(value = "file") MultipartFile file, FileExecuteParam param, HttpServletRequest request)
            throws
            IOException {
        ImportConfigBean configBean = new ImportConfigBean();
        //fileConfigService.getImportConfig(fileExecuteParam.getBizType(), fileExecuteParam.getTaskType());
        FileBean fileBean = new FileBean(file.getOriginalFilename(), file.getContentType(), file.getInputStream());
        //SimpleResult result = new SimpleResult();
        JSONObject data = new JSONObject();

        // 如若后续 分割文件过慢改为异步执行即可
        if (Objects.equals(1, configBean.getSplitFlag())) {
            data.put("tips", importFileReceipt.importSplit(fileBean, param, configBean, request.getRemoteUser()));
        } else {
            data.put("tips", importFileReceipt.importNormal(fileBean, param, configBean, request.getRemoteUser()));
        }
        //result.setData(data);
        return data;
    }

    @RequestMapping(value = "export.do", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject export(@RequestBody FileExecuteParam fileExecuteParam, HttpServletRequest request) {
        ExportTaskBean exportTaskBean = new ExportTaskBean();
        exportTaskBean.setBizType(fileExecuteParam.getBizType());
        exportTaskBean.setTaskType(fileExecuteParam.getTaskType());
        exportTaskBean.setTaskTime(new Date());
        exportTaskBean.setTaskStatus(1);
        exportTaskBean.setTerm(fileExecuteParam.getTerm());
        exportTaskBean.setCreator(request.getRemoteUser());
        exportTaskBean.setUpdater(request.getRemoteUser());
        //导出任务入库
        //Long pkId = exportTaskDao.insertPk(exportTaskBean);
        Long pkId = 11L;
        testExecuteSerivce.export(fileExecuteParam.getBizType(), fileExecuteParam.getTaskType(), pkId);
        //
        //Result result = new SimpleResult();
        JSONObject data = new JSONObject();
        data.put("tips", "导出任务生成成功，请去下载中心查看");
        //result.setData(data);
        return data;
    }
}
