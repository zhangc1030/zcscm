package easyexcel.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileExecuteParam {

    private Integer bizType;

    private Integer taskType;

    private String term;
}
