/*
 *
 * FileName: FileBean.java
 * Author:   wx:fdzhangc
 * Date:     2021/11/25 18:46
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package easyexcel.controller;

import java.io.InputStream;

import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述:<br>
 *
 * @author wx:fdzhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Getter
@Setter
public class FileBean {

    private String originalFileName;

    private String fileName;

    private String fileFormat;

    private String contentType;

    private InputStream inputStream;

    public FileBean(String originalFileName, String contentType, InputStream inputStream) {
        int index = originalFileName.lastIndexOf('.');
        this.fileName = originalFileName.substring(0, index);
        this.fileFormat = originalFileName.substring(index + 1);
        this.originalFileName = originalFileName;
        this.contentType = contentType;
        this.inputStream = inputStream;
    }

    public FileBean(String fileName, String fileFormat, String contentType, InputStream inputStream) {
        this.fileName = fileName;
        this.fileFormat = fileFormat;
        this.originalFileName = fileName + "." + fileFormat;
        this.contentType = contentType;
        this.inputStream = inputStream;
    }
}
