package easyexcel.importexcel;

import java.io.InputStream;

import com.alibaba.excel.EasyExcel;

public class EasyExcelImport {

    public static <T> void load(InputStream inputStream, Class clz, EasyExcelImportListener<T> listener) {
        EasyExcel.read(inputStream, clz, listener).headRowNumber(1).sheet().doRead();
    }

    public static <T> void load(InputStream inputStream, Class clz, Integer headerLine, EasyExcelImportListener<T> listener) {
        EasyExcel.read(inputStream, clz, listener).headRowNumber(headerLine).sheet().doRead();
    }
}
