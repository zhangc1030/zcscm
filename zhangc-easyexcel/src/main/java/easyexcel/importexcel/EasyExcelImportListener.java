package easyexcel.importexcel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

public abstract class EasyExcelImportListener<T> extends AnalysisEventListener<T> {

    private List<T> dataList = new ArrayList<>(1000);

    @Override
    public void invoke(T data, AnalysisContext context) {
        dataList.add(data);

        if (dataList.size() >= 1000) {
            parseData();
            dataList.clear();
        }
    }

    protected abstract void parseData();

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        if (CollectionUtils.isNotEmpty(dataList)) {
            parseData();
            dataList.clear();
        }
    }

    protected List<T> getDataList() {
        return dataList;
    }
}
