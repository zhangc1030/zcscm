package easyexcel.importexcel;

import java.util.List;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportBatchBean<E> {

    private int total;

    private List<E> errorList = Lists.newArrayList();

    public ImportBatchBean(int total) {
        this.total = total;
    }

    public void addError(E error) {
        errorList.add(error);
    }

    public int getErrorCount() {
        return errorList.size();
    }
}
