package easyexcel.importexcel;

import easyexcel.BaseBean;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportTaskBean extends BaseBean {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键 自增主键
     */
    private Long pkId;
    /**
     * 文件名 文件名
     */
    private String fileName;
    /**
     * 文件格式 文件格式
     */
    private String fileFormat;
    /**
     * 业务类型
     */
    private Integer bizType;
    /**
     * 导入类型 1.商品上下架,2.类目关联商品
     */
    private Integer taskType;

    private Integer taskStatus;
    /**
     * 导入时间 导入时间
     */
    private java.util.Date taskTime;
    /**
     * 处理时间 处理时间
     */
    private java.util.Date executeTime;
    /**
     * 总数 总数
     */
    private Integer totalCount;
    /**
     * 成功数 成功数
     */
    private Integer successNum;
    /**
     * 失败数 失败数
     */
    private Integer failNum;
    /**
     * 文件对象Id OSS存储的导入文件对象Id，用于获取文件输入流
     */
    private String fileObjectId;

    /**
     * 发生导入错误的文件id
     */
    private String errorFileObjectId;

    /**
     * 导入条件
     */
    private String term;

}
