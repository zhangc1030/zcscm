package easyexcel.importexcel;

import java.util.List;

public interface EasyImportHandle<E> {

    ImportBatchBean<E> handle(List<E> rowList, ImportTaskBean importTaskBean);
}
