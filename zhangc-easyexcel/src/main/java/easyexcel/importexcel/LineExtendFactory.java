package easyexcel.importexcel;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

@Component
public class LineExtendFactory implements InitializingBean {

    private ImmutableMap<String, LineExtendHandle> extendMap = null;

    // 默认
    private static final LineExtendHandle defaultExtend = new LineExtendHandle() {
        @Override
        public List<List<Object>> apply(List<Object> row) {
            List<List<Object>> rowList = Lists.newArrayList();
            rowList.add(row);
            return rowList;
        }
    };

    @Override
    public void afterPropertiesSet() throws Exception {

    }

    public LineExtendHandle getLineExtendHandle(String key) {
        return extendMap.getOrDefault(key, defaultExtend);
    }
}
