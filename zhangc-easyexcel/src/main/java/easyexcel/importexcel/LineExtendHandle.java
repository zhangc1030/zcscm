package easyexcel.importexcel;

import java.util.List;

public interface LineExtendHandle {

    List<List<Object>> apply(List<Object> row);
}
