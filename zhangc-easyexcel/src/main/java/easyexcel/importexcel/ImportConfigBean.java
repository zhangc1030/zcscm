package easyexcel.importexcel;

import easyexcel.BaseBean;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImportConfigBean extends BaseBean {

    private Integer bizType;

    private String taskId;

    private String taskName;

    // 任务处理的队列名
    private String queueName;

    // 该类任务是否需要定期清理
    private Integer clearFlag = 1;

    // 标题所占行数
    private Integer headerLine = 1;

    // 是否分割文件
    private Integer splitFlag = 0;

    // 多少行分割一次文件 默认2w行
    private Integer splitLine = 20000;

    // 行分割实现类在spring中的bean name
    private String splitHandle;
}
