package easyexcel.importexcel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EasyImportBean<E> {

    /**
     * Import id
     */
    private Long importId;

    /**
     *
     */
    private Class clz;

    /**
     * Handle
     */
    private EasyImportHandle<E> handle;
}
