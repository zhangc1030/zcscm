package easyexcel.exportexcel;

import java.util.List;

public interface EasyExportHandle<E, P> {

    List<E> handle(EasyExcelExport.ExportContext<E, P> context, ExportTaskBean taskBean);
}
