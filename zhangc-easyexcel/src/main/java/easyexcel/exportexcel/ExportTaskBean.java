package easyexcel.exportexcel;

import easyexcel.BaseBean;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExportTaskBean extends BaseBean {

    private Long pkId;

    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件格式
     */
    private String fileFormat;
    /**
     * 业务类型
     */
    private Integer bizType;
    /**
     * 任务类型 1:上下架；2:类目与商品关系
     */
    private Integer taskType;
    /**
     * 导出时间
     */
    private java.util.Date taskTime;
    /**
     * 导出状态 1:下载中；2:下载完成；3:下载失败
     */
    private Integer taskStatus;
    /**
     * 处理时间
     */
    private java.util.Date executeTime;
    /**
     * 文件对象id
     */
    private String fileObjectId;
    /**
     * 查询条件
     */
    private String term;

}
