package easyexcel.exportexcel;

import java.util.List;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EasyExportBean<E, P> {

    /**
     * Export id
     */
    // 导出表的id
    private Long exportId;

    /**
     * Clz
     */
    private Class clz;

    /**
     * File name
     */
    private String fileName;

    /**
     * Sheet name
     */
    private String sheetName;

    /**
     * Header
     */
    // 标题, 如果存在clz没必要设置标题
    private List<List<String>> header;

    /**
     * 分区
     */
    @SuppressWarnings("unchecked")
    private List<P> partition = Lists.newArrayList((P) null);

    /**
     * Handle
     */
    private EasyExportHandle<E, P> handle;
}
