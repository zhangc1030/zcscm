package easyexcel.exportexcel;

import java.util.List;

import com.alibaba.excel.write.metadata.WriteTable;
import com.google.common.collect.Lists;

public interface EasyExportExcelHandle<E, P> {

    // 数据处理导出之前 用于初始化一些准备数据
    default void beforeExportData(EasyExcelExport.ExportContext<E, P> context) {
    }

    // 如果需要自定义列 扩展该方法即可
    default WriteTable getWriteTable() {
        return null;
    }

    @SuppressWarnings("unchecked") // 默认分1片 如果需要多个分片需复写该方法 避免查库深分页 比如按门店维度获取商品 这是可以把所有门店作为分片条件 loadData
    default List<P> getPartition() {
        return Lists.newArrayList((P) null);
    }

    // 获取合计行数据
    default E getSumLine() {
        return null;
    }

    // 初始化合计行数据
    default void initSumLine() {
    }

    // 获取数据 正常来讲只需要复写该方法即可 用于绝大多数场景
    List<E> loadData(EasyExcelExport.ExportContext<E, P> context);

    // 数据处理导出之后 比如上传文件至oss
    default void afterExportData(EasyExcelExport.ExportContext<E, P> context) {
    }
}