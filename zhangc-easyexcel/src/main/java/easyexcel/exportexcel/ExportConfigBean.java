package easyexcel.exportexcel;

import easyexcel.BaseBean;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExportConfigBean extends BaseBean {

    private Integer bizType;

    private Integer taskType;

    private String taskName;

    // 任务处理的队列名
    private String queueName;

    // 该类任务是否需要定期清理
    private Integer clearFlag = 1;

    private Integer pageSize = 1000;

    // sheet页限制最大行数
    private Integer maxSheetLine = 500000;

    // 限制导出条数
    private Integer limitAllSheetLine = 500000;
}
