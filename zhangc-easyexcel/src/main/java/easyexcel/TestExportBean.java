package easyexcel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TestExportBean {
    @ExcelProperty(value = "序号", index = 0)
    private Integer id;

    @ExcelProperty(value = "名称", index = 1)
    private String name;
}
