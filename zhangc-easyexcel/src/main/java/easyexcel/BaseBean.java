package easyexcel;

import java.io.Serializable;
import java.util.Date;

public class BaseBean implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7662695752491764265L;

    /**
     * 创建者
     */
    private String creator;
    /**
     * 更新者
     */
    private String updater;

    /**
     * 创建时间
     */
    private Date createDttm = new Date();
    /**
     * 更新时间
     */
    private Date updateDttm = new Date();

    /**
     * Gets create dttm *
     *
     * @return the createDttm
     */
    public Date getCreateDttm() {
        Date date = createDttm;
        return date;
    }

    /**
     * Sets create dttm *
     *
     * @param createDttm the createDttm to set
     */
    public void setCreateDttm(Date createDttm) {
        if (null != createDttm) {
            this.createDttm = (Date) createDttm.clone();
        } else {
            this.createDttm = null;
        }
    }

    /**
     * Gets update dttm *
     *
     * @return the updateDttm
     */
    public Date getUpdateDttm() {
        Date date = updateDttm;
        return date;
    }

    /**
     * Sets update dttm *
     *
     * @param updateDttm the updateDttm to set
     */
    public void setUpdateDttm(Date updateDttm) {
        if (null != updateDttm) {
            this.updateDttm = (Date) updateDttm.clone();
        } else {
            this.updateDttm = null;
        }
    }

    /**
     * Gets creator *
     *
     * @return the creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * Sets creator *
     *
     * @param creator the creator to set
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * Gets updater *
     *
     * @return the updater
     */
    public String getUpdater() {
        return updater;
    }

    /**
     * Sets updater *
     *
     * @param updater the updater to set
     */
    public void setUpdater(String updater) {
        this.updater = updater;
    }

}
