package com.zhangc.ag;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述:<br>
 * 3，有一个正整数数组，给定一个目标值S，求有多少个连续区间的和大于等于S
 * 例:数组 3，4，7  目标S为7
 * 输出:4
 *
 * @author wx:zhangc
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class alg3 {
    public static void main(String[] args) {
        int[] a = new int[] { 3, 4, 7 };
        int s = 7;

        int len = a.length;
        int count = 0;
        List<String> subList = new ArrayList<>();

        for (int i = 0; i < len; i++) {
            if (a[i] >= s) {
                subList.add(String.valueOf(a[i]));
                count = count + 1;
            }
            int curSum = a[i];

            for (int k = i + 1; k < len; k++) {
                if (curSum + a[k] >= s) {
                    curSum = curSum + a[k];
                    count = count + 1;
                }
            }
        }
        System.out.println(count);
    }
}
