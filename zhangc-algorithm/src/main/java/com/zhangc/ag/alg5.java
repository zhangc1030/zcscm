package com.zhangc.ag;

/**
 * 功能描述:<br>
 * 5，字符串解压缩，有一个字符串，全部由小写英文字符组成，现给出压缩字符串，求其原串
 * 例:4dff
 * 输出:ddddff
 *
 * @author
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class alg5 {
    public static void main(String[] args) {

        String a = "4d4ff";
        System.out.println(parseString(a));
    }

    public static String parseString(String n) {
        int s = n.length();
        //String af = n;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s; i++) {
            char c = n.charAt(i);
            if (Character.isDigit(c)) {
                char c1 = n.charAt(i + 1);
                int countLe = Integer.parseInt(String.valueOf(c));
                for (int m = 0; m < countLe; m++) {
                    sb.append(c1);
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

}
