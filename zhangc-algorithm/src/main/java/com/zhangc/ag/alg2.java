package com.zhangc.ag;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述:<br>
 * 2，有一堆石子，一次编号1-100围成圈，给定一个数K，从1开始数K个，将此石子踢出，继续报数，最后当石子总数小于K时，输出最后剩余石子编号。
 * 例:K为3
 * 输出:58，91
 *
 * @author
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class alg2 {
    public static void main(String[] args) {
        List<Integer> stoneList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            stoneList.add(i);
        }

        int k = 3;

        //1,2,3,4,5---2,4

        //记录退出的人的位置
        int num = 0;
        //写个循环，开始报数，从1~100
        for (int i = 1; i <= stoneList.size(); i++) {
            //当报到第14人的时候，该人退出
            if (i == k) {
                //记录该人的位置
                num = num + (i - 1);
                //如果记录的位置数大于集合的长度
                if (num >= stoneList.size()) {
                    //位置 - 长度
                    num = num - stoneList.size();
                }
                stoneList.remove(num);
                //再从1开始报数
                i = 1;
            }
        }
        //打印剩下的人
        String str = "";
        for (Integer man : stoneList) {
            str += man + ",";
        }
        System.out.println(str.substring(0, str.length() - 1));
    }
}
