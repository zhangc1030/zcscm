package com.zhangc.ag;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述:<br>
 * 6，每个人都有一个能力值，有一个活动要求参赛队伍能力值最低为M，每个队伍可以由1或者2个人组成，求计算最多可以组成多少符合要求的队伍
 * 例:3 1 5 7 9
 * M=8
 * 输出:3
 *
 * @author
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class alg6 {
    public static void main(String[] args) {
        int[] a = new int[] { 3, 1, 5, 7, 9 };
        int m = 8;
        int count = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < a.length; i++) {
            if (a[i] >= m
                    && !map.containsKey(a[i])
                    && !map.containsValue(a[i])) {
                map.put(a[i], 0);
                count++;
            }
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] + a[j] >= m
                        && !map.containsKey(a[i])
                        && !map.containsValue(a[j])) {
                    map.put(a[i], a[j]);
                    count++;
                }
            }
        }

        System.out.println(count);
    }
}
