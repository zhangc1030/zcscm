package com.zhangc.ag;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 功能描述:<br>
 * 有一个整数数组，现在要去掉重复数字，每个数字只保留一个，输出去重后的数组，但是需要按照数字在原数组出现的次数从高到低排序，相同次数的按原顺位输出。
 * 例:1，3，3，3，2，4，4，4，5
 * 输出:3，4，1，2，5
 *
 * @author
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class alg1 {

    public static void main(String[] args) {
        int[] a = new int[] { 1, 3, 3, 3, 2, 4, 4, 4, 5 };
        Map<Integer, Integer> cmap = new HashMap<>();
        for (int i = 0; i < a.length; i++) {
            if (cmap.containsKey(a[i])) {
                cmap.put(a[i], cmap.get(a[i]) + 1);
            } else {
                cmap.put(a[i], 1);
            }
        }
        HashMap<Integer, Integer> finalOut = new LinkedHashMap<>();

        cmap.entrySet()
                .stream()
                .sorted((p1, p2) -> p2.getValue().compareTo(p1.getValue()))
                .collect(Collectors.toList()).forEach(ele -> finalOut.put(ele.getKey(), ele.getValue()));

        System.out.println(finalOut);
    }
}
