# zcscm

#### 介绍
- zhangc-algorithm: 常见算法
- zhangc-batch：异步批量处理：并行请求多个外围接口，互相之间有依赖
- zhangc-easyexcel：easyexcel使用：导入、导出
- zhangc-ratelimiter：单机jvm限流
- zhangc-scm：基于zookeeper的 统一配置管理(初步)
- zhangc-sqldruid：druid sql在线查询
- zhangc-mtlog：美团日志思想https://tech.meituan.com/2021/09/16/operational-logbook.html落地
参考：https://github.com/mouzt/mzt-biz-log




#### 软件架构
springboot


#### 安装教程
maven